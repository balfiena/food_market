part of 'models.dart';

class User extends Equatable {
  final int id; 
  final String name; 
  final String email; 
  final String address; 
  final String houseNumber; 
  final String phoneNumber; 
  final String city; 
  final String picturePath; 

  User(
    {this.id, 
    this.name, 
    this.email, 
    this.address, 
    this.houseNumber, 
    this.phoneNumber, 
    this.city,
    this.picturePath});

  @override
  List<Object> get props => [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User mockUser = User(
  id: 1, 
  name: 'Park Chanyeol', 
  address: 'Jl. D.I Panjaitan', 
  city: 'Palembang', 
  houseNumber: '64', 
  phoneNumber: '082177989065', 
  email: 'chanyeol@exo.com', 
  picturePath: 
  "https://cf.shopee.co.id/file/83f9d63eb999e50427c74c20080985e4"
);