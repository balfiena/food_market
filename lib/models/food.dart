part of 'models.dart';

class Food extends Equatable{
  final int id; 
  final String picturePath; 
  final String name; 
  final String description; 
  final String ingredients; 
  final int price; 
  final double rate; 

  Food(
    {this.id, 
    this.picturePath,
    this.name,
    this.description, 
    this.ingredients,
    this.price,
    this.rate});

  @override
  List<Object> get props => [id, picturePath, name, description, ingredients, price, rate];
}

List<Food> mockFoods =  [
  Food(
    id: 1, 
    picturePath: "https://cf.shopee.co.id/file/83f9d63eb999e50427c74c20080985e4",
    name: "Pempek Kapal Selam", 
    description: "Pempek Kapal Selam adalah makanan khas dari Kota Palembang yang terbuat dari ikan tenggiri dan ditambahkan telur satu butir.",
    ingredients: "Tepung Tapioka, Ikan Tenggiri, Telur, Bumbu Dapur", 
    price: 20000, 
    rate: 4.8), 
    Food(
    id: 2, 
    picturePath: "https://cf.shopee.co.id/file/bd6b51df926005b904e3411783242418",
    name: "Lenggang Panggang", 
    description: "Lenggang Panggang adalah potongan pempek yang dicampur dengan telur, kemudian dipanggang dengan alas daun pisang.",
    ingredients: "Pempek, Telur, Bumbu Dapur", 
    price: 15000,
    rate: 4.5), 
    Food(
    id: 3, 
    picturePath: "https://s3.bukalapak.com/uploads/content_attachment/d189e930f45eddadda09c2b5/original/foto_utama_tekwan.jpg",
    name: "Tekwan", 
    description: "Tekwan adalah bakso ikan yang dibentuk bulat kecil-kecil dilengkapi dengan toping lain seperti jamur kuping.",
    ingredients: "Tepung, Ikan, Bumbu Dapur", 
    price: 10000,
    rate: 4.2), 
    Food(
    id: 4, 
    picturePath: "https://qph.fs.quoracdn.net/main-qimg-f9cf1ba425c7155ed20f247a6a4c9a1c",
    name: "Model", 
    description: "Model adalah bakso ikan namun berbentuk bulat dengan ukuran yang besar dan berisi tahu.",
    ingredients: "Tepung, Ikan, Bumbu Dapur", 
    price: 10000,
    rate: 4.3), 
    Food(
    id: 5, 
    picturePath: "https://disk.mediaindonesia.com/thumbs/600x400/news/2018/01/otak.jpg",
    name: "Otak-Otak", 
    description: "Otak-Otak adalah adonan ikan dan tepung yang dibungkus daun pisang kemudian dibakar di atas bara arang.",
    ingredients: "Tepung, Ikan, Bumbu Dapur", 
    price: 15000,
    rate: 4.5), 
];